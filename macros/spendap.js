const buttondata = [
    { label: "1 AP", length: 1 },
    { label: "2 AP", length: 2 },
    { label: "3 AP", length: 3 },
    { label: "4 AP", length: 4 },
    { label: "5 AP", length: 5 },
    { label: "Dead", length: 80 },
    { label: "-1 AP", length: -1 }
  ];
  
  const buttons = {};
  for (const button of buttondata) {
    buttons[button.label] = {
      label: button.label,
      callback: () => {
        label = button.label;
        length = button.length;
        d.render(true);
      }
    }
  }
  
  let d = new Dialog({
    title: `Spend AP`,
    buttons: buttons,
    close: async html => {
      if (label) {
        if (canvas.tokens.controlled.length == 0) {
          ui.notifications.error("Please select a token");
          return;
        }
        const token = canvas.tokens.controlled[0];
        if (!token.inCombat) {
          ui.notifications.error("The selected token is not in Combat");
          return;
        }
        const cbt = token.combatant;
        console.log("Combatant: ", [cbt, cbt.initiative, length]);
        const speed_factor = cbt.actor.system.dynamic.speed_factor?.system.moddedvalue || 1;
        await cbt.update({
          initiative: cbt.initiative - Math.round(length * 100 / speed_factor),
          ["flags.apcs.actions"]: 1
        });
  
        await cbt.combat.nextTurn();
  
        ChatMessage.create({
          speaker: { actor: cbt.actorId },
          content: `Spent : <b>${label}</b>.`,
        });
        label = "";
      }
    }
  },
    {
      width: 200,
      classes: ["mydialog"],
      top: 0,
      left: 0
    });
  d.render(true);