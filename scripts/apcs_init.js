import { APCS_Combat, APCS_CombatTracker, APCS_Combatant, APCS_CombatantConfig } from "./combat.js";

Hooks.once("init", function () {
  // New User Experience
  game.settings.register("apcs", "shownTips", {
    name: `${game.i18n.localize("NUE.nueName")}`,
    hint: `${game.i18n.localize("NUE.nueHint")}`,
    scope: "world",
    type: Boolean,
    default: false,
    config: true,
    requiresReload: true
  });

  game.settings.register("apcs", "initFromSheet", {
    name: `${game.i18n.localize("initFromSheet.name")}`,
    hint: `${game.i18n.localize("initFromSheet.hint")}`,
    scope: "world",
    type: Boolean,
    default: false,
    config: true,
    requiresReload: true
  });

  CONFIG.Combat.documentClass = APCS_Combat;
  CONFIG.ui.combat = APCS_CombatTracker;
  CONFIG.Combatant.documentClass = APCS_Combatant;
  CONFIG.Combat.sheetClass = APCS_CombatantConfig;
  CONFIG.time.roundTime = 5;

});

Hooks.once("ready", function () {

  /**
   * The New User Experience manager.
   * @type {NewUserExperience}
   */
  Object.defineProperty(this, "apcsnue", {
    value: new APCSNewUserExperience(),
    writable: false,
    enumerable: true,
  });

  // Initialize New User Experience
  this.apcsnue.initialize();

  console.log("Action Point Combat System settings initialized");
});

/**
 * Responsible for managing the New User Experience workflows.
 */
class APCSNewUserExperience {
  /**
   * Initialize the new user experience.
   * Currently, this generates some chat messages with tips for getting started 
   * if we detect this is a new install of the module.
   */
  initialize() {
    // If the tips have not been displayed, this is a new install.
    const isNewWorld = !game.settings.get(
      "apcs",
      "shownTips"
    );
    if (!isNewWorld) return;
    this._createInitialChatMessages();
  }

  /* -------------------------------------------- */

  /**
   * Show chat tips for first launch.
   * @private
   */
  _createInitialChatMessages() {
    if (
      game.settings.get(
        "apcs",
        "shownTips"
      )
    )
      return;

    // Get GM's
    const gms = ChatMessage.getWhisperRecipients("GM");

    // Build Chat Messages
    const content = [
      `
        <h3 class="apcsnue">${game.i18n.localize("NUE.Pane1SystemHeader")}</h3>
        <p class="apcsnue">${game.i18n.localize("NUE.Pane1System")}</p>
        <p class="apcsnue">${game.i18n.localize("NUE.Pane1Features")}</p>
        <footer class="apcsnue">${game.i18n.localize(
        "NUE.FirstLaunchHint"
      )}</footer>
      `,
      `
        <h3 class="apcsnue">${game.i18n.localize("NUE.Pane2GameHeader")}</h3>
        <p class="apcsnue">${game.i18n.localize("NUE.Pane2Game1")}</p>
        <p class="apcsnue">${game.i18n.localize("NUE.Pane2Game2")}</p>
        <p class="apcsnue">${game.i18n.localize("NUE.Pane2Game3")}</p>
        <footer class="apcsnue">${game.i18n.localize(
        "NUE.FirstLaunchHint"
      )}</footer>
      `,
    ];
    const chatData = content.map((c) => {
      return {
        whisper: gms,
        speaker: { alias: game.i18n.localize("APCS") },
        flags: {
          "apcs": {
            apcsnue: true,
            canPopout: true,
          },
        },
        content: c,
      };
    });
    ChatMessage.implementation.createDocuments(chatData);

    // Store flag indicating this was shown
    game.settings.set(
      "apcs",
      "shownTips",
      true
    );
  }
}
