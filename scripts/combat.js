export class APCS_Combat extends Combat {

  async startCombat() {
    await this.setupTurns();
    await this.fetchActorData();
    return super.startCombat();
  }

  async nextTurn() {
    // Determine the next turn number
    let next = -1;
    let found = false;
    // find a non-defeated turn after this one with at least one action remaining
    while (++next < this.turns.length) {
      let t = this.turns[next];
      if (t.defeated) continue;
      if (t.initiative < 100) continue;
      if (t.actor?.statuses.has('defeated')) continue;
      found = true;
      break;
    }

    // the round will not advance if no-one has actions remaining
    if (!found) return this;

    // Update the encounter
    return this.update({ round: this.round, turn: next });
  }

  async nextRound() {
    await this.fetchActorData();
    return super.nextRound();
  }

  async fetchActorData() {
    const fromSheet = game.settings.get("apcs", "initFromSheet");
    // this needs to go after the reset and rollAll or the data gets wiped
    this.combatants.forEach(async c => {
      await c.update({
        ['flags.apcs.ready']: false,
        initiative: c.initiative + (fromSheet ? c.actor.system.bs.value : (c.actor.system.dynamic.apr?.system.moddedvalue || 1)) * 100
      });
      c.actor.resetModVars(true);
      // if the actor has an applyBleeding function, run it
      if (typeof c.actor.applyBleeding === "function") {
        c.actor.applyBleeding();
      }
    });
  }
}

export class APCS_CombatTracker extends CombatTracker {
  get template() {
    return "modules/apcs/templates/combat-tracker.hbs";
  }

  _onConfigureCombatant(li) {
    const combatant = this.viewed.combatants.get(li.data('combatant-id'));
    new APCS_CombatantConfig(combatant, {
      top: Math.min(li[0].offsetTop, window.innerHeight - 350),
      left: window.innerWidth - 720,
      width: 400
    }).render(true);
  }

  async getData(options) {
    const data = await super.getData(options);

    if (!data.hasCombat) {
      return data;
    }

    for (const turn of data.turns) {
      let combatant = this.viewed.combatants.get(turn.id);
      turn.actions = combatant.getFlag("apcs", "actions");
      turn.ready = combatant.getFlag("apcs", "ready");
      turn.canact = combatant.initiative >= 100;
    }
    return data;
  }

  activateListeners(html) {
    super.activateListeners(html);
    html.find(".interrupt").click(this._onInterrupt.bind(this));
    html.find(".spend").click(this._onSpend.bind(this));
    html.find(".regain").click(this._onRegain.bind(this));
    html.find(".initiative").change(this._onInitiativeChanged.bind(this));
    html.find(".name").click(this._onToggleReady.bind(this));
  }

  async _onToggleReady(event) {
    const btn = event.currentTarget;
    const li = btn.closest(".combatant");
    const c = this.viewed.combatants.get(li.dataset.combatantId);
    if (!c.isOwner) return;
    await c.setFlag("apcs", "ready", !c.getFlag("apcs", "ready"));
  }

  async _onSpend(event) {
    const btn = event.currentTarget;
    const li = btn.closest(".combatant");
    const c = this.viewed.combatants.get(li.dataset.combatantId);
    if (!c.isOwner) return;
    await c.update({ ["flags.apcs.actions"]: c.getFlag("apcs", "actions") - 1 });
  }

  async _onRegain(event) {
    const btn = event.currentTarget;
    const li = btn.closest(".combatant");
    const c = this.viewed.combatants.get(li.dataset.combatantId);
    if (!c.isOwner) return;
    await c.update({ ["flags.apcs.actions"]: c.getFlag("apcs", "actions") + 1 });
  }

  async _onInitiativeChanged(event) {
    const btn = event.currentTarget;
    const li = btn.closest(".combatant");
    const c = this.viewed.combatants.get(li.dataset.combatantId);
    if (!c.isOwner) return;
    await c.update({
      initiative: btn.value,
      ["flags.apcs.actions"]: 1
    });
  }

  async _onInterrupt(event) {
    const btn = event.currentTarget;
    const li = btn.closest(".combatant");
    const c = this.viewed.combatants.get(li.dataset.combatantId);
    if (!c.isOwner) return;
    const speed_factor = c.actor.system.dynamic.speed_factor?.system.moddedvalue || 1;
    await c.update({
      initiative: c.initiative - Math.round(c.getFlag("apcs", "actions") * 100 / speed_factor),
      ["flags.apcs.actions"]: 1
    });
  }
}

export class APCS_CombatantConfig extends CombatantConfig {
  get template() {
    return "modules/apcs/templates/combatant-config.hbs";
  }
}

export class APCS_Combatant extends Combatant {
  _onCreate(data, options, userId) {
    super._onCreate(data, options, userId);
    if (this.isOwner)
      this.update({
        initiative: null,
        ['flags.apcs.ready']: false,
        ['flags.apcs.actions']: 1
      });
  }
}
